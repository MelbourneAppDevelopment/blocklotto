// -------   React UI   -------
// Enter 'npm run start' to run
// ----------------------------

import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import web3 from "./web3";
import blockLotto from "./blocklotto";

class App extends Component {

   
    state = {
      administrator: '',
      gamblers: [],
      balance: '0',
      adminFee: '0',
      value: '',
      message: ''
     }; // Initial state of manager
  
  async componentDidMount() {
    const administrator = await blockLotto.methods.administrator().call(); // always call from account-1
    const gamblers = await blockLotto.methods.getGamblerList().call();
    const balance = await web3.eth.getBalance(blockLotto.options.address);
    const adminFee = await blockLotto.methods.adminFee().call();

    this.setState({ administrator, gamblers, balance, adminFee }); // Same a SetPropertyChanged
  }

  onSubmit = async event => {
    event.preventDefault();

    const accounts = await web3.eth.getAccounts();

    this.setState({ message: 'Waiting ...' });

    console.log("TEST 1");
    console.log(this.state.value);
    console.log(web3.utils.toWei(this.state.value, 'ether'));
    console.log(accounts[0]);

    console.log(blockLotto.methods.administrator().call());
    
    await blockLotto.methods.enterLotto().send({
      from: accounts[0],
      value: web3.utils.toWei(this.state.value, 'ether')
    });
console.log("TEST 2");

    this.setState({ message: 'You are now playing.' });
  };

  onClick = async () => {
console.log("DRAW 1");
const accounts = await web3.eth.getAccounts();

    this.setState({ message: 'Waiting ..' });
    console.log("DRAW 2");

    await blockLotto.methods.drawLotto().send({
      from: accounts[0]
    });
    console.log("DRAW 3");

    this.setState({ message: 'The winnings have been sent' });
console.log("DRAW 4");
};

  render() {
    // web3.eth.getAccounts().then(console.log);

    // renderer is listing for manager state changed
    return (
      <div>
      <h2>Block Lotto</h2>
      <p><b>
      Lotto Administrator: {this.state.administrator}
      </b></p>
      <table>
        <tbody>
             <tr>
              <td>Current entries:</td>
              <td>{this.state.gamblers.length}</td> 
              <td></td> 
            </tr>
            <tr>
              <td>Current balance:</td>
              <td>{web3.utils.fromWei(this.state.balance, 'ether')}</td> 
              <td> ether</td> 
            </tr>
            <tr>
              <td>Of which:</td>
              <td>{this.state.adminFee}</td> 
              <td> is fees</td> 
            </tr>
         </tbody>
     </table>
 
      <hr />

      <form onSubmit={this.onSubmit}>
         <div>
          <label>Enter ether to win: </label>
          <input
            value={this.state.value}
            onChange={event => this.setState({ value: event.target.value })}
          />
        </div>
        <button>Enter</button>
      </form>

      <hr />

      <h4>Ready to draw lotto?</h4>
      <button onClick={this.onClick}>Draw Lotto!</button>

      <hr />

      <h1>{this.state.message}</h1>
    </div>

 
    );
  }
}

export default App;
