import Web3 from 'web3';
const web3 = new Web3(window.web3.currentProvider); // Grabs provider form metaMask's injected provider.
export default web3;
