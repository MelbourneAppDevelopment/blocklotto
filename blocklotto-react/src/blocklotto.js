// --- Contract addess and ABI ---
// Get these as output from the deploy script.

import web3 from "./web3";
const address = "0xC174acc328a44Ab7374a0B8b5231Bd09306850b5";      // 12/10/2018
// const address = "0xf2695dC8A6D5B1bf9D761f3f6b8d75050E381E5f";  // 17/10/2018
const abi = [{ "constant": true, "inputs": [], "name": "getGamblerList", "outputs": [{ "name": "", "type": "address[]" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "pool", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "drawLotto", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "uint256" }], "name": "gamblers", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "enterLotto", "outputs": [], "payable": true, "stateMutability": "payable", "type": "function" }, { "constant": true, "inputs": [], "name": "adminFee", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "administrator", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }]

;

export default new web3.eth.Contract(abi, address); // Portal to our rinkeby contract
