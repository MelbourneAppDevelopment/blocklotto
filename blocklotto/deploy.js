// Deploying to a real test network (rather than ganache)
// Difference is:
//   - provider must have a 'real' account with ether
//   - 
console.log('Starting');

const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile');

const provider = new HDWalletProvider(
  'act poet start autumn gallery start decrease glide into mother ozone produce',   // Put your account Mnemonic here
  'https://rinkeby.infura.io/v3/da1a8acd876c4f41b203fd95121b3d77'                   // Node to connect to. Normally set up our own.
  // This node come from infura. 
);
const web3 = new Web3(provider);

const deploy = async () => {
  console.log('Getting accounts');
  let accounts;
  try {
    accounts = await web3.eth.getAccounts();
  } catch (err) {
    console.log('Failed to connect. Are you connected?');
    exit();
  }

  console.log('Attempting to deploy from account', accounts[0]);

  const result = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: '0x' + bytecode })
    .send({ gas: '2500000', from: accounts[0] });

  console.log( interface);
  console.log('Contract deployed to', result.options.address);
};
deploy();

// Contract deployed to 0xb34e7282e6B1990D9939762D7Dde590d86Be9c54
// use remix.ethereum.org to interact with it.