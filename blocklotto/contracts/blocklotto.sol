pragma solidity ^0.4.17;

contract blocklotto {
    address public administrator;
    address[] public gamblers;
    uint public pool;
    uint public adminFee;
    
    constructor() public {
        administrator = msg.sender;
        adminFee = 0;
        gamblers = new address[](0);     
        
    }
    
    function enterLotto() public payable {
        require(msg.value > .01 ether);
        gamblers.push(msg.sender);
    //     uint balance = address(this).balance;
    //     adminFee = balance / 1000;             // 0.1% admin costs
    //     // pool = balance - adminFee;
    //    return;
    }
    
    function drawLotto() public mustBeAdministrator {
        uint index = uint(keccak256( now)) % gamblers.length;     // Simple 'random' algorithm
        administrator.transfer(adminFee);        
        gamblers[index].transfer(address(this).balance);
        gamblers = new address[](0);                              // reset gambler list
        adminFee = 0;
        pool = 0;
    }
    
    modifier mustBeAdministrator() { 
        require(msg.sender == administrator);
        _;
    }

   function getGamblerList() public view returns (address[])
    {
        return gamblers;
    }
}