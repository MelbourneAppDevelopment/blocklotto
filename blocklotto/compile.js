// ---- Compile script ----
// Run with 'node compile.js'
// -------------------------

const path = require('path');       // 'path' plugin to give us cross platform compatability
const fs = require('fs');           // Similar for file handling
const solc = require('solc');       

const sourcePath = path.resolve(__dirname, 'contracts', 'blocklotto.sol');     // '__dirname' and standard node variable
const source = fs.readFileSync(sourcePath, 'utf8');

// console.log(solc.compile(source, 1));                                           // Use this for clean compile      
module.exports = solc.compile(source, 1).contracts[':blocklotto'];              // Use this to export into test scripts and deploy
