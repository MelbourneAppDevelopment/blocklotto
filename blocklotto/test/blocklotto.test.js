//  ----  Test Script  ----
//  Defined in package.json.
//  Make sure the compile.js finishes with 'module.export' rather than 'console.log'.
//  Run with 'npm run test'
//  -----------------------


const assert = require('assert');
const ganache = require('ganache-cli');       // Tools for creating our local test network.
const Web3 = require('web3');                 // Libraries that give us programatic access to Ethereum using the ABI
const web3 = new Web3(ganache.provider());    // We are going to use the ganache provider rather than the Rinkeby provider i.e. which network.

const { interface, bytecode } = require('../compile');

let blockLotto;
let accounts;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  blockLotto = await new web3.eth.Contract(JSON.parse(interface))
    .deploy({ data: bytecode })
    .send({ from: accounts[0], gas: '3000000' });
});

describe('Block Lotto', () => {
  it('Show what we have got', () => {
    console.log("\nABI Interface:");
    console.log(JSON.parse(interface));

    console.log("\nByte code:");
    console.log(bytecode);

    console.log("\nTwo account codes:");
    console.log(accounts[0]);
    console.log(accounts[1]);

  });



  it('deploys the contract ok', () => {
    assert.ok(blockLotto.options.address);
  });

  it('allows one account to enter', async () => {
    await blockLotto.methods.enterLotto().send({
      from: accounts[0],
      value: web3.utils.toWei('0.02', 'ether')
    });

    const gamblers = await blockLotto.methods.getGamblerList().call({
      from: accounts[0]
    });

    assert.equal(accounts[0], gamblers[0]);
    assert.equal(1, gamblers.length); // Order is: should be value, is value
  });

  it('specific enterLotto values', async () => {
    await blockLotto.methods.enterLotto().send({
      from: accounts[0],
      value: 100000000000000000
    });

    const gamblers = await blockLotto.methods.getGamblerList().call({
      from: accounts[0]
    });

    assert.equal(accounts[0], gamblers[0]);
    assert.equal(1, gamblers.length); // Order is: should be value, is value
  });

 it('allows multiple accounts to enter', async () => {
    await blockLotto.methods.enterLotto().send({
      from: accounts[0],
      value: web3.utils.toWei('0.03', 'ether')
    });
    await blockLotto.methods.enterLotto().send({
      from: accounts[1],
      value: web3.utils.toWei('0.03', 'ether')
    });
    await blockLotto.methods.enterLotto().send({
      from: accounts[2],
      value: web3.utils.toWei('0.03', 'ether')
    });

    const gamblers = await blockLotto.methods.getGamblerList().call({
      from: accounts[0]
    });

    assert.equal(accounts[0], gamblers[0]);
    assert.equal(accounts[1], gamblers[1]);
    assert.equal(accounts[2], gamblers[2]);
    assert.equal(3, gamblers.length);
  });

  it('requires a minimum amount of ether to enter', async () => {
    try {
      await blockLotto.methods.enterLotto().send({
        from: accounts[0],
        value: 0
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('only administrator can call draw a winner', async () => {
    try {
      await blockLotto.methods.drawLotto().send({
        from: accounts[1]
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('sends money to the winner and resets the gambler array', async () => {
    await blockLotto.methods.enterLotto().send({
      from: accounts[0],
      value: web3.utils.toWei('2', 'ether')
    });

    const initialBalance = await web3.eth.getBalance(accounts[0]);
    await blockLotto.methods.drawLotto().send({ from: accounts[0] });
    const finalBalance = await web3.eth.getBalance(accounts[0]);
    const difference = finalBalance - initialBalance;
    assert(difference > web3.utils.toWei('1.8', 'ether'));
  });
});
